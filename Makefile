
all:
	eval $$(opam env) && dune build src

build-deps:
	scripts/build-deps.sh

clean:
	dune clean
	find -name dune-project -exec rm {} \;

clean-dist:
	dune clean
	rm -Rf _opam
